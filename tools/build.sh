#!/usr/bin/env bash
function prep_nexus() {
    mkdir -p build/nexus/ship_variation_expansion_vro
}

function prep_steam() {
    mkdir -p build/steam/ship_variation_expansion_vro
}

function clean_nexus() {
    rm -rf build/nexus
}

function clean_steam() {
    rm -rf build/steam
}

function build_nexus() {
    echo "Building catalog and data files....."  
    wine $HOME/.steam/debian-installation/steamapps/common/X\ Tools/XRCatTool.exe -in ./ -out ./build/nexus/ship_variation_expansion_vro/ext_01.cat -exclude "^.git*" -exclude ".dae" -exclude "README.md" -exclude "content.xml" -exclude "tools/" -exclude "build/" -exclude "xmllib/"
    sleep 1

    echo "Copying content.xml into build dir...."
    cp content.xml build/nexus/ship_variation_expansion_vro/content.xml
    cd build/nexus
    sleep 1

    echo "Compressing into zip file...."
    zip -r ship_variation_expansion_vro.zip ship_variation_expansion_vro/
    cd ..

    echo "Nexus mod archive created!"
}

function build_steam() {
    echo "Building catalog and data files for steam...."
    wine ${CATTOOL} -in ./ -out ./build/steam/ship_variation_expansion_vro/ext_01.cat -exclude "^.git*" -exclude ".dae" -exclude "README.md" -exclude "content.xml" -exclude "tools/" -exclude "*.cat" -exclude "*.dat" -exclude "build/" -exclude "xmllib/"

    echo "Copying content XML to steam dir...."
    cp content.xml build/steam/ship_variation_expansion_vro/content.xml

    echo "Done and ready for Workshop Upload!"
}

function nexus() {
    clean_nexus
    prep_nexus
    build_nexus
}

function steam() {
    clean_steam
    prep_steam
    build_steam
}

function all() {
    nexus
    steam
}

function clean() {
    rm -rf build/*
}

case "$1" in
    "steam")
        steam
        ;;
    "nexus")
        nexus
        ;;
    "all")
        all
        ;;
    "clean")
        clean
        ;;
    *)
        echo "Not a build command."
        exit 1
        ;;
esac
